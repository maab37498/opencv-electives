#pragma once
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <iostream>
#include "opencv2/opencv.hpp"
#include <string>

using namespace std;
using namespace cv;


int xmas_video(string location) {
	VideoCapture cap("C:/Program Files (x86)/picture-cpp/xmas-tree-video.mp4");

	int frame_width = cap.get(CAP_PROP_FRAME_WIDTH);
	int frame_height = cap.get(CAP_PROP_FRAME_HEIGHT);

	VideoWriter video("C:/Program Files (x86)/picture-cpp/xmas-tree-video.mp4", VideoWriter::fourcc('M', 'J', 'P', 'G'), 25, Size(frame_width, frame_height));

	while (1) {

		Mat frame;
		cap >> frame;

		if (frame.empty()) {           // Break if no frame
			break;
		}

		Mat grayscale;
		cvtColor(frame, grayscale, COLOR_BGR2GRAY);

		frame = grayscale;

		medianBlur(frame, frame, 5);	// Median blur with kernel size of 5
		vector<Vec3f> circles;		// Vector of 3 floating point integers	
		HoughCircles(frame, circles,  // circles: A vector that stores sets of 3 values: xc,yc,r for each detected circle.
			HOUGH_GRADIENT,// Detection method - HOUGH_GRADIENT is the only one as of OpenCV 4.6.0
			1.1,				// Inverse ratio of resolution 
			frame.rows / 12,   // Minimum distance in pixels between the circles
			95,			// Upper threshold for internal Canny edge detector
			29,			// Threshold for center detection
			10,			// Minimum radius to be detected. If unknown, set to zero.
			40			// Maximum radius to be detected. If unknown, set to zero.
		);

		for (size_t i = 0; i < circles.size(); i++)
		{
			Vec3i c = circles[i];	// Convert circles vector to hold integers.
			Point center = Point(c[0], c[1]); // Define center points as Point object
			circle(frame, center, 5, Scalar(128, 0, 128), 3, LINE_AA); // Define center dot size and color.
			int radius = c[2]; // Find radius of enclosing circles
			circle(frame, center, radius, Scalar(255, 0, 255), 3, LINE_AA); // Draw gray circles around detected circles
		}

		Mat colorFrame;
		cvtColor(frame, colorFrame, COLOR_GRAY2BGR);

		video.write(colorFrame);
		imshow("Frame", frame);

		char c = (char)waitKey(25);
		if (c == 27)
			break;
	}

	cap.release();
	cv::destroyAllWindows();
	return 0;
}
