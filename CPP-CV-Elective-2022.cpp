#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include <iostream>
#include "basic.h"
#include "analysis.h"
#include "thresholding.h"
#include "chains.h"
#include "filters.h"
#include "ex1.h"
#include "keypoint_matching.h"
#include "detectors.h"
#include "video_tree.h"

#include <opencv2/core/utils/logger.hpp>

using namespace cv;
using namespace std;

Mat invert_image(Mat const& input)
{
	return 255 - input;
}

int main(int argc, char** argv) {
	utils::logging::setLogLevel(cv::utils::logging::LogLevel::LOG_LEVEL_SILENT);
	Mat image = imread("C:/Program Files (x86)/picture-cpp/christmas-ornaments-box.jpg", IMREAD_GRAYSCALE);
	//image = resize_image(image, 640, 480);
	//Mat temp = imread("Tajmahal2.webp");
	//temp = resize_image(temp, 640, 480);
	//Mat dilated = preprocessing(image);
	//circle_detector(image);
	//i_am_blrrr(image);
	//get_dimensions(image);
	//getContourAreas(dilated, image);
	//image = contours_full(image);
	//Mat edges = CannyThreshold(image);
	//imshow("Detected edges", edges);
	//waitKey();
	//video_edge_detection();
	//matchContoursFull(image, temp);
	//Blob_detector(image);


	//image = invert_image(image);

	//int number = circle_detector(image);
	//cout << "Number of the decoration balls: " << number;

	//sift_matching(image, temp);

	xmas_video("C:/Program Files (x86)/picture-cpp/xmas-tree-video.mp4");

	//imwrite("C:C:/Program Files (x86)/picture-cpp/video1.avi", image);


}

/*
int main(int argc, char** argv) {
	Mat image = imread("christmas-ornaments-box.jpg", IMREAD_GRAYSCALE);
	medianBlur(image, image, 5);	// Median blur with kernel size of 5
	vector<Vec3f> circles;		// Vector of 3 floating point integers
	HoughCircles(image, circles,  // circles: A vector that stores sets of 3 values: xc,yc,r for each detected circle.
		HOUGH_GRADIENT,// Detection method - HOUGH_GRADIENT is the only one as of OpenCV 4.6.0
		1,				// Inverse ratio of resolution
		image.rows / 16,   // Minimum distance in pixels between the circles
		100,			// Upper threshold for internal Canny edge detector
		30,			// Threshold for center detection
		1,				// Minimum radius to be detected. If unknown, set to zero.
		70				// Maximum radius to be detected. If unknown, set to zero.
	);

	for (size_t i = 0; i < circles.size(); i++)
	{
		Vec3i c = circles[i];	// Convert circles vector to hold integers.
		Point center = Point(c[0], c[1]); // Define center points as Point object
		circle(src, center, 5, Scalar(128, 0, 128), 3, LINE_AA); // Define center dot size and color.
		int radius = c[2]; // Find radius of enclosing circles
		//circle(src, center, radius, Scalar(255, 0, 255), 3, LINE_AA); // Draw gray circles around detected circles
	}
	imshow("Detected circles", image);
	waitKey();
}
*/
